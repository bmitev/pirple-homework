/*
 * Module for handling incoming request data and routing it to the respective API
 */

// Dependencies
const http = require('http');
const https = require('https');
const url  = require('url');

const { environment } = require('./lib/config');
const handlers = require('./lib/handlers');
const connection = require('./lib/connection');

// HTTP / HTTPS server configuration and initialization
const httpsOptions = {
   key: environment.httpsKey,
   cert: environment.httpsCert
}
const httpServer = http.createServer(serverListener);
const httpsServer = https.createServer(httpsOptions, serverListener);

httpServer.listen(environment.httpPort, () => console.log(`HTTP server is now listening on port ${environment.httpPort}`));
httpsServer.listen(environment.httpsPort, () => console.log(`HTTPS server is now listening on port ${environment.httpsPort}`));

// Handle incoming HTTP / HTTPS server requests
async function serverListener(request, response) {
   let { pathname: path, query } = url.parse(request.url, true);
   let { method, headers } = request;

   path = path.replace(/^\/+|\/+$/g, '');
   method = method.toLowerCase();

   const data = await connection.getData(request);
   const parameters = { path, query, method, headers, data };

   const handler = handlers[path] || handlers.notFound;

   handler(parameters, (code = 200, data = {}) => {
      response.setHeader('content-type', 'application/json');
      response.writeHead(code);
      response.end(JSON.stringify(data));
   });
}
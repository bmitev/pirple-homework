# Homework Assignment 2

Implementaion notes:

- ES6 syntax is used to implement the Promise-based APIs.
- JSON web tokens are used for user validation.
- All APIs require a valid token so they can provide data related to the authenticated user only.
- Payment details are not required when creating a new user. They are hardcoded.
- An existing, valid e-mail should be provided when creating new users.
- The user ID is a hash of the e-mail as it is considered unique and is used as a key.
- Editing individual user details is allowed, including the e-mail.
- Editing the user e-mail will change the user ID as well, including the user ID inside related existing orders.
- Editing any user details will invalidate all existing tokens for that user.

To setup a working Pizza store you should:

1. Create a new user `Users > Create new user`.
2. Get a token for that user `Tokens > Get token for an existing user`.
3. Create some items using the token `Items > Create item`.
4. List all items and pick a few `Items > List item IDs`.
5. Put the selected items in the cart `Cart > Put item in cart by ID`.
6. Submit an order `Orders > Submit order`.
7. Expire the token `Tokens > Expire all tokens for an existing user`.

## Users API

Only registered users can receive valid tokens to work with all the APIs.

#### Create new user
- Method: `POST`
- URL: `https://localhost:3001/user`
- Headers: `Content-Type: application/json`
- Body:
```JSON
{
	"firstName": "First name",
	"lastName": "Last name",
	"address": "Some address",
	"email": "valid@mail.com",
	"password": "passw0rd"
}
```

#### Get existing user data
- Method: `GET`
- URL: `https://localhost:3001/user`
- Headers: `token: TOKEN`

#### Edit existing user data
- Method: `PUT`
- URL: `https://localhost:3001/user`
- Headers: `Content-Type: application/json; token: TOKEN`
- Body:
```JSON
{
   "lastName": "Other last name"
}
```

#### Delete existing user
- Method: `DELETE`
- URL: `https://localhost:3001/user`
- Headers: `token: TOKEN`



## Tokens API

JSON web tokens issued to registered users, so they can edit their own user information, manage items and use the shopping cart to place orders. A valid token should be sent with every request, requiring authentication, as a token header (`token: TOKEN`).

#### Get token for an existing user
- Method: `POST`
- URL: `https://localhost:3001/token`
- Headers: `Content-Type: application/json`
- Body:
```JSON
{
	"email": "valid@mail.com",
	"password": "passw0rd"
}
```

#### Expire all tokens for an existing user
- Method: `DELETE`
- URL: `https://localhost:3001/token`
- Headers: `token: TOKEN`


## Items API

Items are available for purchasing. They can be managed using a valid token.

#### Create item

- Method: `POST`
- URL: `https://localhost:3001/items`
- Headers: `Content-Type: application/json; token: TOKEN`
- Body:
```JSON
{
   "name": "Pepperoni",
   "category": "Pizza",
   "description": "Salami, mozzarella, tomatos",
   "price": 9.95
}
```

#### Get item by ID

- Method: `GET`
- URL: `https://localhost:3001/items?id=ID`
- Headers: `token: TOKEN`

#### List item IDs

- Method: `GET`
- URL: `https://localhost:3001/items`
- Headers: `token: TOKEN`


## Cart API

Every registered user has their own shopping cart, which they can fill with items of their choice. A shopping cart with items is a prerequisite for placing an order. The shopping cart can be managed using a valid token.

#### Put item in cart by ID

- Method: `PUT`
- URL: `https://localhost:3001/items?id=ID`
- Headers: `token: TOKEN`

#### Remove item from cart by ID

- Method: `DELETE`
- URL: `https://localhost:3001/items?id=ID`
- Headers: `token: TOKEN`

#### Empty cart

- Method: `DELETE`
- URL: `https://localhost:3001/items`
- Headers: `token: TOKEN`

#### List item IDs in cart

- Method: `GET`
- URL: `https://localhost:3001/items`
- Headers: `token: TOKEN`


## Orders API

Orders are created by taking the shopping cart content, creating a new order for it, calculating the total amount due, sending a payment request for that amount and a notification about the new purchase to the user's e-mail. Credit card data is hardcoded. Orders can be managed using a valid token.

#### Submit order

- Method: `POST`
- URL: `https://localhost:3001/orders`
- Headers: `token: TOKEN`

#### List order IDs

- Method: `GET`
- URL: `https://localhost:3001/orders`
- Headers: `token: TOKEN`

#### Get order by ID

- Method: `GET`
- URL: `https://localhost:3001/orders?id=ID`
- Headers: `token: TOKEN`

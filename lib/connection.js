/*
 * Module for managing HTTP / HTTPS connections to external APIs and parsing response data
 */

 // Dependencies
const https = require('https');
const querystring = require('querystring');
const { StringDecoder } = require('string_decoder');

const helpers = require('./helpers');

// Export module
module.exports = new function Connection() {
    this.request = request;
    this.getData = getData;
};


// Send request to external API
async function request(method, url, data) {
   const { protocol, hostname, pathname: path, username, password } = new URL(url);
   const auth = `${username}:${password}`;
   const payload = querystring.stringify(data);
   const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(payload)
   };
   const parameters = { method, protocol, auth, hostname, path, headers };

   return sendRequest(parameters, payload)
      .then(response => getData(response))
      .then(data => resolve(data))
      .catch(() => reject('Error sending request.'));
}

// Send HTTPS request
async function sendRequest(parameters, payload) {
   return new Promise((resolve, reject) => {
      const request = https.request(parameters, response => {
         response.statusCode === 200 && resolve(response) || reject();
      });
      request.on('error', () => reject());
      request.write(payload);
      request.end();
   });   
}

// Get data from HTTP / HTTPS response
async function getData(response) {
   const decoder = new StringDecoder('utf-8');
   const headers = response.headers || {};
   let buffer = '';

   return new Promise(resolve => {
      response.on('data', data => buffer += decoder.write(data));
      response.on('end', () => {
         buffer += decoder.end();
         
         let data = buffer;
         if (headers['content-type'] === 'application/json') {
            data = helpers.parseJSON(data) || {};
         }
         resolve(data);
      });
   });
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}
/*
 * Module for handling incoming request parameters and routing them to the respective API
 */

// Dependencies
const api = require('./api');

// Export module
module.exports = new function Handlers() {
   this.token = token;
   this.user = user;
   this.cart = cart;
   this.orders = orders;
   this.items = items;
   this.notFound = notFound;
};


// Route parameters to Token API methods
function token(parameters, callback) {
   const { method, headers, data } = parameters;
   const { email, password } = data;
   const { token } = headers;

   switch(method) {
      case 'post':
         api.token.create(email, password)
            .then(token => callback(200, { token }))
            .catch(error => callback(500, { error }));
         break;
      case 'delete':
         api.token.expire(token)
            .then(() => callback(200))
            .catch(error => callback(500, { error }));
         break;
      default:
         callback(404);
         break;
   }
}

// Route parameters to User API methods
function user(parameters, callback) {
   const { method, headers, data } = parameters;
   const { token } = headers;

   switch(method) {
      case 'post':
         api.user.add(data)
            .then(() => callback(200))
            .catch(error => callback(500, { error }));
         break;
      case 'get':
         api.token.decode(token)
            .then(token => api.user.get(token))
            .then(user => callback(200, { user }))
            .catch(error => callback(500, { error }));
         break;
      case 'put':
         api.token.decode(token)
            .then(token => api.user.update(token, data))
            .then(() => callback(200))
            .catch(error => callback(500, { error }));
         break;
      case 'delete':
         api.token.decode(token)
            .then(token => api.user.remove(token))
            .then(() => callback(200))
            .catch(error => callback(500, { error }));
         break;
      default:
         callback(404);
         break;
   }
}

// Route parameters to Cart API methods
function cart(parameters, callback) {
   const { method, query, headers } = parameters;
   const { token } = headers;
   const { id } = query;

   switch(method) {
      case 'put':
         api.token.decode(token)
            .then(token => api.cart.add(token, id))
            .then(() => callback(200))
            .catch(error => callback(500, { error }));
         break;
      case 'get':
         api.token.decode(token)
            .then(token => api.cart.list(token))
            .then(cart => callback(200, { cart }))
            .catch(error => callback(500, { error }));
         break;
      case 'delete':
         if (id) {
            api.token.decode(token)
               .then(token => api.cart.remove(token, id))
               .then(() => callback(200))
               .catch(error => callback(500, { error }));
         }
         else {
            api.token.decode(token)
               .then(token => api.cart.empty(token))
               .then(() => callback(200))
               .catch(error => callback(500, { error }));
         }
         break;
      default:
         callback(404);
         break;
   }
}

// Route parameters to Orders API methods
function orders(parameters, callback) {
   const { method, query, headers } = parameters;
   const { token } = headers;
   const { id } = query;

   switch(method) {
      case 'post':
         api.token.decode(token)
            .then(token => api.orders.add(token))
            .then(order => callback(200, { order }))
            .catch(error => callback(500, { error }));
         break;
      case 'get':
         if (id) {
            api.token.decode(token)
               .then(token => api.orders.get(token, id))
               .then(order => callback(200, { order }))
               .catch(error => callback(500, { error }));
            break;
         }
         else {
            api.token.decode(token)
               .then(token => api.orders.list(token))
               .then(orders => callback(200, { orders }))
               .catch(error => callback(500, { error }));
            break;
         }
      default:
         callback(404);
         break;
   }
}

// Route parameters to Items API methods
function items(parameters, callback) {
   const { method, query, headers, data } = parameters;
   const { token } = headers;
   const { id } = query;

   switch(method) {
      case 'post':
         api.token.decode(token)
            .then(() => api.items.add(data))         
            .then(() => callback(200))
            .catch(error => callback(500, { error }));
         break;
      case 'get':
         if (id) {
            api.token.decode(token)
               .then(() => api.items.get(id))         
               .then(item => callback(200, { item }))
               .catch(error => callback(500, { error }));
         }
         else {
            api.token.decode(token)
               .then(() => api.items.list())         
               .then(items => callback(200, { items }))
               .catch(error => callback(500, { error }));
         }
         break;
      default:
         callback(404);
         break;
   }
}

// Page not found handler
function notFound(parameters, callback) {
   callback(404);
}

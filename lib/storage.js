/*
 * Module for storing and managing data
 */

// Dependencies
const fs = require('fs');
const path = require('path');
const helpers = require('./helpers');

const rootPath = path.join(__dirname, '/../.data');

// Export module
module.exports = new function Storage() {
   this.add = add;
   this.get = get;
   this.list = list;
   this.update = update;
   this.rename = rename;
   this.remove = remove;
};


// Write data to a file
async function add(folderName, fileName, fileData) {
   const file = {
      path: `${rootPath}/${folderName}/${fileName}.json`,
      data: JSON.stringify(fileData),
      options: { encoding: 'utf8', flag: 'wx' }
   };

   return fs.promises.writeFile(file.path, file.data, file.options)
      .catch(() => reject(`Error writing to new file: ${file.path}`));
};

// Read data from a file
async function get(folderName, fileName) {
   const file = {
      path: `${rootPath}/${folderName}/${fileName}.json`,
      options: { encoding: 'utf8' }
   };

   return fs.promises.readFile(file.path, file.options.encoding)
      .then(data => resolve(helpers.parseJSON(data)))
      .catch(() => reject(`Error reading from existing file: ${file.path}`));
};

// Update data in a file
async function update(folderName, fileName, fileData) {
   const file = {
      path: `${rootPath}/${folderName}/${fileName}.json`,
      data: fileData,
      options: { encoding: 'utf8', flag: 'w+' }
   };

   return fs.promises.readFile(file.path, file.options.encoding)
      .then(data => fs.promises.writeFile(file.path, JSON.stringify({ ...helpers.parseJSON(data), ...file.data }), file.options))
      .catch(() => reject(`Error updating existing file: ${file.path}`));
};

// Rename a file
async function rename(folderName, currentFileName, newFileName) {
   const file = {
      currentPath: `${rootPath}/${folderName}/${currentFileName}.json`,
      newPath: `${rootPath}/${folderName}/${newFileName}.json`,
      options: { encoding: 'utf8', flag: 'wx' }
   };

   return fs.promises.readFile(file.currentPath, file.options.encoding)
      .then(data => fs.promises.writeFile(file.newPath, data, file.options))
      .then(() => fs.promises.unlink(file.currentPath))
      .catch(() => reject(`Error renaming existing file: ${file.currentPath}`));
}

// Delete a file
async function remove(folderName, fileName) {
   const file = {
      path: `${rootPath}/${folderName}/${fileName}.json`
   };

   return fs.promises.unlink(file.path)
      .catch(() => reject(`Error removing existing file: ${file.path}`));
}

// List all files in a folder
async function list(folderName) {
   const folder = {
      path: `${rootPath}/${folderName}/`,
      pattern: /\.json$/
   };

   return fs.promises.readdir(folder.path)
      .then(fileList => fileList.filter(fileName => fileName.match(folder.pattern)).map(fileName => fileName.replace(folder.pattern, '')))
      .catch(() => reject(`Error listing existing files in folder: ${folder.path}`));
};

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

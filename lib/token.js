
/*
 * Module for creating and decoding JSON web tokens
 */

// Dependencies
const helpers = require('./helpers');

// Export module
module.exports = new function Token() {
   this.create = create;
   this.decode = decode;
};


// Create token based on user data
async function create(user = { id, firstName, lastName, email }) {
   const { id, firstName, lastName, email } = user;

   const date = Date.now();
   const data = {
      header: {
         typ: 'JWT',
         alg: 'SHA256'
      },
      payload: {
         issuer: 'Pirple App',
         created: date,
         expires: date + 1000 * 60 * 60,
         user: { id, firstName, lastName, email }
      }
   }
   const header = toBase64(data.header);
   const payload = toBase64(data.payload);
   const signature = helpers.hashSHA256(`${header}.${payload}`);

   return resolve(`${header}.${payload}.${signature}`);
}

// Decode valid token
async function decode(token = '') {
   let data = token.split('.');

   if (data.length === 3) {
      const [ header, payload, signature ] = data;
      const hash = helpers.hashSHA256(`${header}.${payload}`);

      if (signature === hash) {
         data = {
            header: fromBase64(header),
            payload: fromBase64(payload)
         };
         return resolve(data);
      }
   }
   return reject('Invalid token.');
}

// Convert object to base64 string
function toBase64(object = {}) {
   return Buffer.from(JSON.stringify(object)).toString('base64');
}

// Convert base64 string to object
function fromBase64(string = '') {
   return helpers.parseJSON(Buffer.from(string, 'base64').toString('utf8'));
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

/*
 * Module with applicaton configuration 
 */

// Dependencies
const fs  = require('fs');
const path = require('path');

const rootPath = path.join(__dirname, '/https');

// Get environment type
const { NODE_ENV: environmentType = '' } = process.env;

// Allowed environment types
const environmentTypes = {
   production: 'production',
   staging: 'staging'
};

// Environment congifurations
const environments = {
   [environmentTypes.production]: {
      type: environmentTypes.production,
      httpPort: 5000,
      httpsPort: 5001,
      httpsKey: fs.readFileSync(`${rootPath}/key.pem`),
      httpsCert: fs.readFileSync(`${rootPath}/cert.pem`)
   },
   [environmentTypes.staging]: {
      type: environmentTypes.staging,
      httpPort: 3000,
      httpsPort: 3001,
      httpsKey: fs.readFileSync(`${rootPath}/key.pem`),
      httpsCert: fs.readFileSync(`${rootPath}/cert.pem`)
   }
};

// Export module
module.exports = {
   environmentTypes,
   environment: environments[environmentType.toLowerCase()] || environments[environmentTypes.staging],
   hashingSecret: 'myH@sh1ngS3cr3t',
   emailPattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
   stripe: {
      url: 'https://sk_test_4eC39HqLyjWDarjtT1zdp7dc@api.stripe.com/v1/charges',
      currency: 'USD'
   },
   mailgun: {
      url: 'https://api:9d3337383ec1d4e70f109afd9a7ce1fe-de7062c6-ab125a69@api.mailgun.net/v3/sandboxbf1156a74591462b83fcad87cc203f1b.mailgun.org/messages',
      subject: 'Order completed successfully',
      from: {
         name: 'Pizza Delivery',
         email: 'postmaster@sandboxbf1156a74591462b83fcad87cc203f1b.mailgun.org'
      }
   }
};

/*
 * API module for managing orders 
 */

// Dependencies
const helpers = require('../helpers');
const storage = require('../storage');
const payment = require('./payment');
const notification = require('./notification');

// Export module
module.exports = new function Orders() {
   this.add = add;
   this.get = get;
   this.list = list;
};

// Add new user order
async function add(token) {
   const userId = token.payload.user.id;

   return storage.get('users', userId)
      .then(user => create(user))
      .then(order => resolve(order))
      .catch(() => reject('Could not add order. User cart might be empty or order payment might have failed.'));
}

// Get existing user order data
async function get(token, id) {
   id = typeof(id) === 'string' && id.length === 36 && id || null;

   if (!id) {
      return reject('Could not get order. Invalid order id.');
   }
   const userId = token.payload.user.id;

   return storage.get('orders', id)
      .then(order => verify(order, userId))
      .then(order => resolve(order))
      .catch(() => reject('Could not get order. Order might not exist.'));
}

// List all user orders
async function list(token) {
   const userId = token.payload.user.id;

   return storage.get('users', userId)
      .then(user => resolve(user.orders))
      .catch(() => reject('Could not list orders. User might not exist.'));
}

// Create user order from cart, calculate total amount, send payment and notification e-mail
async function create(user) {
   if (user.cart.length === 0) {
      return Promise.reject();
   }
   const id = helpers.createGUID();
   const created = Date.now();

   const promises = user.cart.map(item => storage.get('items', item));
   const items = await Promise.all(promises);

   const amount = items.reduce((result, item) => result + item.price, 0);

   let text = 'Ordered items\n\n'
   text += items.reduce((result, item) => `${result}${item.name} - ${item.price} USD\n`, '');
   text += `\nTotal - ${amount.toFixed(2)} USD`;

   const parameters = { id, user: user.id, items: user.cart, amount, created };   

   return storage.add('orders', id, parameters)
      .then(() => storage.update('users', user.id, { cart: [], orders: user.orders.concat(id) }))
      .then(() => payment.send(id, amount, 'Order payment'))
      .then(payment => storage.update('orders', id, { payment }))
      .then(() => notification.send(user.firstName, user.lastName, user.email, text))
      .then(notification => storage.update('orders', id, { notification }))
      .then(() => storage.get('orders', id));
}

// Verify order by user id
async function verify(order, userId) {
   return order.user === userId && Promise.resolve(order) || Promise.reject();
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

/*
 * Module exposing all public APIs
 */

// Dependencies
const token = require('./token');
const user = require('./user');
const cart = require('./cart');
const items = require('./items');
const orders = require('./orders');

// Export module
module.exports = new function API() {
   this.token = token;
   this.user = user;
   this.cart = cart;
   this.items = items;
   this.orders = orders;
};
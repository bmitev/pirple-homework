/*
 * API module for managing payments 
 */

// Dependencies
const connection = require('../connection');
const config = require('../config');

// Export module
module.exports = new function Payment() {
   this.send = send;
};


// Send payment request to Stripe API
async function send(id, amount, description, source = 'tok_visa') {
   const parameters = {
      'source': source,
      'currency': config.stripe.currency,
      'amount': Math.round(amount * 100),
      'description': description,
      'metadata[id]': id
   };
   return connection.request('POST', config.stripe.url, parameters)
      .then(payment => getData(payment))
      .then(data => resolve(data))
      .catch(() => reject('Stripe API error. Service might be down.'));
}

// Get payment data from Stripe API payment response
async function getData(payment) {
   const { id, description, amount, currency, status, metadata } = payment;
   const data = { id, description, amount: amount / 100, currency, status, metadata };
   return Promise.resolve(data);
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}
/*
 * API module for managing mail notifications 
 */

// Dependencies
const connection = require('../connection');
const config = require('../config');

// Export module
module.exports = new function Notification() {
   this.send = send;
};


// Send mail notification request to Mailgun API
async function send(firstName, lastName, email, text) {
   const parameters = {
      'from': `${config.mailgun.from.name} <${config.mailgun.from.email}>`,
      'to': `${firstName} ${lastName} <${email}>`,
      'subject': config.mailgun.subject,
      'text': text
   };
   return connection.request('POST', config.mailgun.url, parameters)
      .then(mail => getData(mail))
      .then(data => resolve(data))
      .catch(() => reject('Mailgun API error. Service might be down.'));
}

// Get mail notification data from Mailgun API response
async function getData(mail) {
   const { id, message: status } = mail;
   const data = { id, status };
   return Promise.resolve(data);
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}
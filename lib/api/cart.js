/*
 * API module for managing shopping cart data 
 */

// Dependencies
const storage = require('../storage');

// Export module
module.exports = new function Cart() {
   this.add = add;
   this.list = list;
   this.remove = remove;
   this.empty = empty;
};


// Add item to shopping cart
async function add(token, id) {
   id = typeof(id) === 'string' && id.length === 36 && id || null;

   if (!id) {
      return reject('Could not add item to cart. Invalid item id.');
   }
   return storage.get('items', id)
      .then(() => storage.get('users', token.payload.user.id))
      .then(user => storage.update('users', user.id, { ...user, cart: user.cart.concat(id) }))
      .then(() => resolve())
      .catch(() => reject('Could not add item to cart. Item or user might not exist.'));   
}

// List shopping cart content
async function list(token) {
   return storage.get('users', token.payload.user.id)
      .then(user => resolve(user.cart))
      .catch(() => reject('Could not get cart items. User might not exist.'));
}

// Remove item from shopping cart
async function remove(token, id) {
   id = typeof(id) === 'string' && id.length === 36 && id || null;

   if (!id) {
      return reject('Could not remove item from cart. Invalid item id.');
   }
   return storage.get('users', token.payload.user.id)
      .then(user => storage.update('users', user.id, { ...user, cart: user.cart.filter(item => item != id) }))
      .then(() => resolve())
      .catch(() => reject('Could not remove item from cart. User might not exist.'));
}

// Empty shopping cart content
async function empty(token) {
   return storage.get('users', token.payload.user.id)
      .then(user => storage.update('users', user.id, { ...user, cart: [] }))
      .then(() => resolve())
      .catch(() => reject('Could not empty cart. User might not exist.'));
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

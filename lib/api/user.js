/*
 * API module for managing incoming and stored user data 
 */

// Dependencies
const config = require('../config')
const helpers = require('../helpers');
const storage = require('../storage');

// Export module
module.exports = new function User() {
   this.add = add;
   this.get = get;
   this.update = update;
   this.remove = remove;
};


// Add new user data
async function add(data) {
   let { firstName, lastName, address, email, password } = data;

   firstName = typeof(firstName) === 'string' && firstName.trim() || null;
   lastName = typeof(lastName) === 'string' && lastName.trim() || null;
   address = typeof(address) === 'string' && address.trim() || null;
   email = typeof(email) === 'string' && config.emailPattern.test(email.trim()) && email.trim() || null;
   password = typeof(password) === 'string' && password.trim() && helpers.hashSHA256(password.trim()) || null;

   if (!firstName || !lastName || !address || !email || !password) {
      return reject('Could not add user. Invalid user first name, last name, address, e-mail or password.');
   }
   const hash = helpers.hashMD5(email);
   const modified = Date.now();
   const cart = [];
   const orders = [];

   const parameters = { id: hash, firstName, lastName, address, email, password, cart, orders, modified };

   return storage.add('users', hash, parameters)
      .then(() => resolve())
      .catch(() => reject('Could not add user. User might already exist.'));
}

// Get existing user data
async function get(token) {
   const id = token.payload.user.id;

   return storage.get('users', id)
      .then(user => filter(user))
      .then(data => resolve(data))
      .catch(() => reject('Could not get user. User might not exist.'));
}

// Update existing user data
async function update(token, data) {
   let { firstName, lastName, address, email, password } = data;

   firstName = typeof(firstName) === 'string' && firstName.trim() || null;
   lastName = typeof(lastName) === 'string' && lastName.trim() || null;
   address = typeof(address) === 'string' && address.trim() || null;
   email = typeof(email) === 'string' && config.emailPattern.test(email.trim()) && email.trim() || null;
   password = typeof(password) === 'string' && password.trim() && helpers.hashSHA256(password.trim()) || null;

   if (!firstName && !lastName && !address && !email && !password) {
      return reject('Could not update user. Invalid user first name, last name, address, e-mail and password.');
   }
   const id = token.payload.user.id;
   const hash = helpers.hashMD5(email);
   const modified = Date.now();
   
   let parameters = { id: hash, firstName, lastName, address, email, password, modified };

   // Leave only valid parameters for updating
   parameters = Object.keys(parameters).reduce((result, key) => {
      if (parameters[key]) result[key] = parameters[key];
      return result;
   }, {});

   if (hash && hash !== id) {
      // Update user data and rename user data file to match new e-mail
      return storage.rename('users', id, hash)
         .then(() => storage.update('users', hash, parameters))
         .then(() => storage.get('users', hash))
         .then(user => Promise.all(user.orders.map(order => storage.update('orders', order, { user: hash }))))
         .then(() => resolve())
         .catch(() => reject('Could not update user. User might not exist or e-mail might be in use.'));
   }
   else {
      // Update user data
      return storage.update('users', id, parameters)
         .then(() => resolve())
         .catch(() => reject('Could not update user. User might not exist.'));
   }
}

// Remove existing user data
async function remove(token) {
   const id = token.payload.user.id;

   return storage.remove('users', id)
      .then(() => resolve())
      .catch(() => reject('Could not remove user. User might not exist.'));
}

// Filter out protected user properties
async function filter(user) {
   const { id, firstName, lastName, address, email } = user;

   return Promise.resolve({ id, firstName, lastName, address, email });
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

/*
 * API module for creating, decoding and expiring JSON web tokens
 */

// Dependencies
const token = require('../token');
const helpers = require('../helpers');
const storage = require('../storage');

// Export module
module.exports = new function Token() {
   this.create = create;
   this.decode = decode;
   this.expire = expire;
};


// Create token string based on valid email and password
async function create(email, password) {
   return validateUser(email, password)
      .then(user => token.create(user))
      .then(string => resolve(string))
      .catch(() => reject('Invalid email or password.'));
}

// Decode token string and verify it has not yet expired
async function decode(string) {
   return validateToken(string)
      .then(data => resolve(data))
      .catch(() => reject('Expired token.'));
}

// Update user data modification date to expire all old tokens
async function expire(string) {
   return validateToken(string)
      .then(data => expireToken(data))
      .then(() => resolve())
      .catch(() => reject('Failed to expire token. Token might have already expired or user might not exist.'));
}

// Verify the token string expires in the future and has been created after the last user data modification
async function validateToken(string) {
   return token.decode(string)
      .then(data => {
         let { created, expires, user } = data.payload;
         return storage.get('users', user.id)
            .then(user => created > user.modified && expires > Date.now() && Promise.resolve(data) || Promise.reject());
      });
}

// Verify user email and password
async function validateUser(email, password) {
   return storage.get('users', helpers.hashMD5(email))
      .then(user => {
         const hash = helpers.hashSHA256(password);
         return hash === user.password && Promise.resolve(user) || Promise.reject();
      });
}

// Update user modification date to expire old tokens
async function expireToken(data) {
   let { id } = data.payload.user;
   return storage.get('users', id)
      .then(user => storage.update('users', id, { ...user, modified: Date.now() }));
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

/*
 * API module for managing item data 
 */

// Dependencies
const helpers = require('../helpers');
const storage = require('../storage');

// Export module
module.exports = new function Items() {
   this.add = add;
   this.get = get;
   this.list = list;
};


// Add new item data
async function add(data) {
   let { name, category, description, price } = data;

   name = typeof(name) === 'string' && name.trim() || null;
   category = typeof(category) === 'string' && category.trim() || null;
   description = typeof(description) === 'string' && description.trim() || null;
   price = typeof(price) === 'number' && price > 0 && price || null;

   if (!name || !category || !description || !price) {
      return reject('Could not add item. Invalid item name, category, description or price.');
   }
   const id = helpers.createGUID();

   const parameters = { id, name, category, description, price };

   return storage.add('items', id, parameters)
      .then(() => resolve())
      .catch(() => reject('Could not add item. Item might already exist.'));
}

// Get existing item data
async function get(id) {
   id = typeof(id) === 'string' && id.length === 36 && id || null;

   if (!id) {
      return reject('Could not get item. Invalid item id.');
   }
   return storage.get('items', id)
      .then(item => resolve(item))
      .catch(() => reject('Could not get item. Item might not exist.'));
}

// List all items
async function list() {
   return storage.list('items')
      .then(items => resolve(items))
      .catch(() => reject('Could not list items. Items might not exist.'));
}

// Resolve promise
async function resolve(data) {
   return Promise.resolve(data);
}

// Reject promise and log data
async function reject(data) {
   data && console.log(data);
   return Promise.reject(data);
}

/*
 * Module with various helper tools
 */

// Dependencies
const crypto = require('crypto');
const config = require('./config')

// Export module
module.exports = new function Helpers() {
   this.hashMD5 = hashMD5;
   this.hashSHA256 = hashSHA256;
   this.createGUID = createGUID;
   this.parseJSON = parseJSON;
};


// Parse JSON string to object without throwing on error
function parseJSON(string) {
   try {
      return JSON.parse(string);
   }
   catch (exception) {
      return null;
   }
}

// Create a SHA256 hash
function hashSHA256(string){
   if (string && typeof(string) === 'string'){
      return crypto.createHmac('sha256', config.hashingSecret)
         .update(string)
         .digest('hex');
   }
   else {
      return null;
   }
}

// Create a MD5 hash
function hashMD5(string){
   if (string && typeof(string) === 'string'){
      return crypto.createHash('md5')
         .update(string)
         .digest('hex');
   }
   else {
      return null;
   }
}

// Create a GUID
function createGUID() {
   var hex = Date.now().toString(16) + Math.random().toString(16).substring(2) + '0'.repeat(25);
   return `${hex.substr(0, 8)}-${hex.substr(8, 4)}-4000-8${hex.substr(12, 3)}-${hex.substr(15, 12)}`;
}
